#!/bin/bash
database=$1
query=$2
threads=$3
output=$4

pblat $database $query -threads=$threads -tileSize=12 -stepSize=7 -minScore=23 -oneOff=1 -repMatch=1000000 -fine $output
