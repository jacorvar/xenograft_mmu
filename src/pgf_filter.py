import numpy as np
import sys

# Read file storing probes to exclude
fname_probes = sys.argv[1]
#fname_probes = 'excluded_probes.txt'
probes = open(fname_probes, 'r')
probelist = [line.split()[0] for line in probes.xreadlines()]
probelist = list(set(probelist))

# Read pgf file
fname_pgf = sys.argv[2]
#fname_pgf = 'Clariom_D_Human.r1.pgf'
pgf = open(fname_pgf, 'r')
pgflist = np.array([line.split('\t') for line in pgf.xreadlines()])

# Get lines storing probes
idx1 , bool1 = zip(*[(idx, True) if (len(p) == 8) & (p[0].find('#%') < 0) else (idx, False) for idx, p in enumerate(pgflist)])
idx1 = np.array(idx1)
bool1 = np.array(bool1, dtype=bool)

# Extract lines whose probe_id is in list of probes to exclude
idx2 = np.in1d(np.vstack(pgflist[bool1])[:, 2], probelist)

# Remove lines whose probe_id is in list of probes to exclude
idx3 = idx1[bool1][idx2]
pgflist = np.delete(pgflist, idx3, 0)

# Filter out absent probe pairs
arrlen = np.array([1 if (len(p) == 2) & (p[0].find('#%') < 0) else -1 for p in pgflist.tolist() ])
arrlen_idx = arrlen + np.append(arrlen, -1)[1:]
arrlen_idx = arrlen_idx <= 0
pgflist = pgflist[arrlen_idx]
#for p in pgflist:
#    print '\t'.join(p)

with open(fname_pgf, 'w') as f:
	f.writelines('\t'.join(p) for p in pgflist)
