#!/bin/bash
SHELL:=/bin/bash
DATADIR := $(CURDIR)/data
SRCDIR := $(CURDIR)/src
#URL_MMU_TRANSCRIPTOME := ftp://ftp.ncbi.nih.gov/genomes/refseq/vertebrate_mammalian/Mus_musculus/latest_assembly_versions/GCF_000001635.25_GRCm38.p5/GCF_000001635.25_GRCm38.p5_rna.fna.gz
PROBES := $(DATADIR)/Clariom_D_Human.hg38.main.probes.tar.gz
QUERY := $(basename $(basename $(PROBES))).fa
PROBE_TYPE := "main->psr"
#GZ_MMU_TRANSCRIPTOME := $(DATADIR)/GCF_000001635.25_GRCm38.p5_rna.fna.gz
#DATABASE := $(basename $(GZ_MMU_TRANSCRIPTOME))
GZ_DATABASE := $(DATADIR)/GCF_000001635.25_GRCm38.p5_rna.fna.tar.gz
DATABASE := $(basename $(basename $(GZ_DATABASE)))
SGE := 0
THREADS := 8
PBLAT_OUTPUT := $(DATADIR)/pblat_output.psl
EXCLUDED_PROBES := $(DATADIR)/excluded_probes.tab
GROUPING_PROBES := $(DATADIR)/tc_pb_p_grouping.tab
TOTAL_EXCLUDED_PROBES := $(basename $(EXCLUDED_PROBES)).rds
TOTAL_EXCLUDED_PROBES_TXT := $(basename $(EXCLUDED_PROBES)).txt
FREQTAB_SCRIPT := $(SRCDIR)/tc_pb_p_freqtab.R

ARRAY_NAME := $(DATADIR)/Clariom_D_Human
ANNOT_PSR := $(ARRAY_NAME).na36.hg38.probeset.csv
ANNOT_TC := $(ARRAY_NAME).na36.hg38.transcript.csv
CLF_FILE := $(ARRAY_NAME).r1.clf.gz
PGF_FILE := $(ARRAY_NAME).r1.pgf.gz
MPS_FILE := $(ARRAY_NAME).r1.mps.gz
ARRAY_FILES := clf pgf mps
PKG_NAME := pd.clariom.d.human
#PKG_NEW_NAME := $(PKG_NAME).xenograft_mmu
#PKG_DESCRIPTION := $(PKG_NEW_NAME)/DESCRIPTION
#PKG_DESCRIPTION_VER = $(shell grep Version $(PKG_DESCRIPTION) | cut -f2 -d' ')

$(QUERY): $(PROBES)
	tar -xvf $< -O | grep -v "#%" | grep $(PROBE_TYPE) | awk '{id=$$4" | "$$2" | "$$1; print ">"id"\n"$$5}' > $(QUERY)

$(DATABASE): $(GZ_DATABASE)
	tar xvf $< -C $(@D)

#$(GZ_MMU_TRANSCRIPTOME):
#	wget -N $(URL_MMU_TRANSCRIPTOME) -O $@


# if SGE equals 1, use pblat on the HPC to get the alignments of probes to transcripts
ifeq ($(strip $(SGE)),1)
SCRIPT = /usr/bin/qsub $(SRCDIR)/pblat_xenograft.qsub
else 
SCRIPT = /bin/bash $(SRCDIR)/pblat_xenograft.sh
endif

$(PBLAT_OUTPUT): $(DATABASE) $(QUERY)
	$(SCRIPT) $^ $(THREADS) $(PBLAT_OUTPUT)

$(EXCLUDED_PROBES): $(PBLAT_OUTPUT)
	tail -n +6 $< | cut -f10 | sort -u > $@

$(GROUPING_PROBES): $(PROBES)
	tar zxvf $< -O | grep -v "#%" | grep $(PROBE_TYPE) | cut -f1,2,4 > $@

$(TOTAL_EXCLUDED_PROBES): $(EXCLUDED_PROBES) $(GROUPING_PROBES)
	Rscript $(FREQTAB_SCRIPT) $^

$(ARRAY_NAME).r1.clf: $(TOTAL_EXCLUDED_PROBES) $(ARRAY_NAME).r1.clf.gz
	zgrep "#%" $@.gz > $@; \
	Rscript $(SRCDIR)/probe_filter.R $@.gz $<

$(ARRAY_NAME).r1.mps: $(TOTAL_EXCLUDED_PROBES) $(ARRAY_NAME).r1.mps.gz
	cut -f3 $(TOTAL_EXCLUDED_PROBES_TXT) | sort -u | tr '\n' '|' | sed -e 's/|$$//' | awk '{print "\""$$0"\""}' | grep -v -E -f - <(zcat $@.gz) > $@

$(ARRAY_NAME).r1.pgf: $(TOTAL_EXCLUDED_PROBES_TXT) $(ARRAY_NAME).r1.pgf.gz
	zcat $@.gz > $@; \
	python $(SRCDIR)/pgf_filter.py $< $@

$(ANNOT_PSR): $(TOTAL_EXCLUDED_PROBES_TXT)
	cut -f2 $(TOTAL_EXCLUDED_PROBES_TXT) | sort -u | tr '\n' '|' | sed -e 's/|$$//' | awk '{print "\""$$0"\""}' | grep -v -E -f - <(tar xf $@.tar.gz -O) > $@

$(ANNOT_TC): $(TOTAL_EXCLUDED_PROBES_TXT)
	cut -f3 $(TOTAL_EXCLUDED_PROBES_TXT) | sort -u | tr '\n' '|' | sed -e 's/|$$//' | awk '{print "\""$$0"\""}' | grep -v -E -f - <(tar xf $@.tar.gz -O) > $@

ARRAY_FILES := clf pgf mps

$(PKG_NAME): $(addprefix $(ARRAY_NAME).r1.,$(ARRAY_FILES)) $(ANNOT_PSR) $(ANNOT_TC)
	Rscript $(SRCDIR)/make_xeno_lib.R $^

#$(PKG_NEW_NAME): $(PKG_NAME)
#	mv $< $@

#$(PKG_DESCRIPTION): $(PKG_NEW_NAME)
#	sed -i '/DBI/a \\t pd.clariom.d.human (>= $(PKG_DESCRIPTION_VER)),' $(PKG_DESCRIPTION)

$(PKG_NAME).tar.gz: $(PKG_NAME)
	tar zcvf $@ $<  --remove-files

all: $(PKG_NAME).tar.gz

commit: all
	git add $(PKG_NAME).tar.gz; \
	git add $(TOTAL_EXCLUDED_PROBES); \
	git add $(CURDIR)/prop.excluded.probes.png; \
	git add $(CURDIR)/Rplots.pdf; \
	git commit -m "Add Package File and Excluded probes"

clean: 
	rm -f $(QUERY) $(DATABASE) $(PBLAT_OUTPUT) $(EXCLUDED_PROBES) $(GROUPING_PROBES) $(TOTAL_EXCLUDED_PROBES_TXT) \
	$(ARRAY_NAME).r1.clf $(ARRAY_NAME).r1.mps $(ARRAY_NAME).r1.pgf $(ANNOT_PSR) $(ANNOT_TC)


.PHONY: all clean commit
